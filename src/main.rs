mod quadtree;
mod shape;
mod vector2;
use self::quadtree::{QuadTree, AABB};
use self::shape::Shape;

fn main() {
    let rect_left_up = Shape::new_rect(10.0, 60.0, 15.0, 70.0, 0);
    let circle_left_up = Shape::new_circle(40.0, 90.0, 2.0, 1);
    let circle_left_down = Shape::new_circle(35.0, 30.0, 5.0, 2);
    let circle_right_up = Shape::new_circle(60.0, 80.0, 5.0, 3);
    let line_right_down = Shape::new_line(65.0, 20.0, 75.0, 30.0, 4);

    let mut qt : QuadTree<Shape> = QuadTree::new(AABB::new(50.0, 50.0, 50.0, 50.0));
    qt.insert(&line_right_down);
    qt.insert(&rect_left_up);
    qt.insert(&circle_left_up);
    qt.insert(&circle_left_down);
    qt.insert(&circle_right_up);

    println!("Shapes in range of {}", rect_left_up);
    let in_range : Vec<_> = qt.query(&rect_left_up).collect();
    in_range.iter().for_each(|shape| println!("{}", shape));
    println!("Done!");

    println!("");
    // There is no gonna be any intersections since they are
    // on same cuadrant but not intersecting
    for (id, shapes) in shape::find_intersections(in_range) {
        for shape in shapes {
            println!("{}: {}", id, shape);
        }
    }
}
