// Based on https://github.com/dmac/rust-quadtree/blob/master/src/quadtree.rs
// But the code is old Rust, and won't compile. Plus no need to fight borrow
// checker as much.

use crate::vector2::Vector2;

pub struct QuadTree<'a, T> {
    capacity: usize,
    depth: usize,
    max_depth: usize,
    elements: Vec<&'a T>,
    area: AABB,
    children: Option<[Box<QuadTree<'a, T>>; 4]>, // TODO Try with &mut 'a
}

/// Following Unity conventions
/// https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Bounds.cs
#[derive(PartialEq, Debug)]
pub struct AABB {
    pub center: Vector2,
    pub extents: Vector2,
}

impl AABB {
    pub fn new(center_x: f64, center_y: f64, extents_x: f64, extents_y: f64) -> Self {
        let center = Vector2::new(center_x, center_y);
        let extents = Vector2::new(extents_x, extents_y);
        AABB { center, extents }
    }

    pub fn min_corner(&self) -> Vector2 {
        Vector2::new(
            self.center.x - self.extents.x,
            self.center.y - self.extents.y,
        )
    }

    pub fn max_corner(&self) -> Vector2 {
        Vector2::new(
            self.center.x + self.extents.x,
            self.center.y + self.extents.y,
        )
    }

    pub fn contains<T: Bounded>(&self, other: &T) -> bool {
        let other = other.bounds();
        self.min_corner().x <= other.min_corner().x
            && self.min_corner().y <= other.min_corner().y
            && self.max_corner().x >= other.max_corner().x
            && self.max_corner().y >= other.max_corner().y
    }
}

pub trait Bounded {
    fn bounds(&self) -> AABB;
}

#[derive(PartialEq, Eq)]
enum Quadrant {
    TopRight,
    BotRight,
    BotLeft,
    TopLeft,
}

impl Quadrant {
    pub fn quad(&self) -> usize {
        match self {
            Quadrant::TopRight => 0,
            Quadrant::BotRight => 1,
            Quadrant::BotLeft => 2,
            Quadrant::TopLeft => 3,
        }
    }
}

impl<'a, T: Bounded> QuadTree<'a, T> {
    pub fn new(area: AABB) -> QuadTree<'a, T> {
        QuadTree {
            capacity: 4,
            depth: 0,
            max_depth: 10,
            elements: Vec::new(),
            area,
            children: None,
        }
    }

    // Builder pattern for depth parameter
    pub fn depth(self, depth: usize) -> Self {
        QuadTree { depth, ..self }
    }

    pub fn insert(&mut self, element: &'a T) {
        match (self.get_cuadrant(element), &mut self.children) {
            // Children fits in some quadrant, recursive case
            (Some(ref quad), Some(children)) => {
                children[quad.quad()].insert(element);
            }
            // No quadrant & children
            // There is no split since even though we may be over
            // capacity there is no quadrant that fits
            (None, Some(_)) => {
                self.elements.push(element); // Why this weird borrow?
            }
            // No children, this only happens first time we access a node
            (_, None) => {
                self.elements.push(element);
                if self.elements.len() > self.capacity {
                    self.split();
                }
            }
        }
    }

    pub fn split(&mut self) {
        if self.depth == self.max_depth {
            return;
        }
        match self.children {
            Some(_) => unreachable!(),
            None => {
                let area = &self.area;
                let mut children: [Box<QuadTree<T>>; 4] = [
                    Box::new(
                        QuadTree::new(AABB {
                            center: Vector2::new(
                                area.center.x + area.extents.x / 2.0,
                                area.center.y + area.extents.y / 2.0,
                            ),
                            extents: area.extents * 0.5,
                        })
                        .depth(self.depth + 1),
                    ),
                    Box::new(
                        QuadTree::new(AABB {
                            center: Vector2::new(
                                area.center.x - area.extents.x / 2.0,
                                area.center.y + area.extents.y / 2.0,
                            ),
                            extents: area.extents * 0.5,
                        })
                        .depth(self.depth + 1),
                    ),
                    Box::new(
                        QuadTree::new(AABB {
                            center: Vector2::new(
                                area.center.x + area.extents.x / 2.0,
                                area.center.y - area.extents.y / 2.0,
                            ),
                            extents: area.extents * 0.5,
                        })
                        .depth(self.depth + 1),
                    ),
                    Box::new(
                        QuadTree::new(AABB {
                            center: Vector2::new(
                                area.center.x - area.extents.x / 2.0,
                                area.center.y - area.extents.y / 2.0,
                            ),
                            extents: area.extents * 0.5,
                        })
                        .depth(self.depth + 1),
                    ),
                ];
                let mut unfit_elements: Vec<&T> = Vec::new();
                for &element in self.elements.iter() {
                    match self.get_cuadrant(element) {
                        Some(quadrant) => children[quadrant as usize].insert(element), // TODO: Change this
                        None => unfit_elements.push(element),
                    };
                }

                self.children = Some(children);
                self.elements = unfit_elements;
            }
        };
    }

    /// Returns whichever quadrants a bounded objects fits in (if it fits)
    fn get_cuadrant(&self, elem: &T) -> Option<Quadrant> {
        let bounds = elem.bounds();

        let fits_left_half = bounds.min_corner().x >= self.area.min_corner().x
            && bounds.max_corner().x <= self.area.center.x;
        let fits_right_half = bounds.min_corner().x >= self.area.center.x
            && bounds.max_corner().x <= self.area.max_corner().x;
        let fits_bottom_half = bounds.min_corner().y >= self.area.min_corner().y
            && bounds.max_corner().y <= self.area.center.y;
        let fits_top_half = bounds.min_corner().y >= self.area.center.y
            && bounds.max_corner().y <= self.area.max_corner().y;

        if fits_top_half && fits_right_half {
            Some(Quadrant::TopRight)
        } else if fits_bottom_half && fits_right_half {
            Some(Quadrant::BotRight)
        } else if fits_bottom_half && fits_left_half {
            Some(Quadrant::BotLeft)
        } else if fits_top_half && fits_left_half {
            Some(Quadrant::TopLeft)
        } else {
            None
        }
    }

    pub fn query(&self, element: &'a T) -> QueryItems<T> {
        QueryItems {
            tree: self,
            idx: 0,
            element,
            future_trees: Vec::new(),
        }
    }
}

pub struct QueryItems<'a, T> {
    tree: &'a QuadTree<'a, T>,
    idx: usize,
    element: &'a T,
    future_trees: Vec<&'a QuadTree<'a, T>>,
}

impl<'a, T: Bounded> Iterator for QueryItems<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<&'a T> {
        // Current node elements
        if self.idx < self.tree.elements.len() {
            let elem = self.tree.elements[self.idx];
            self.idx += 1;
            return Some(elem);
        }

        match (self.tree.get_cuadrant(self.element), &self.tree.children) {
            // elements fits in quad & node has children
            (Some(ref quad), Some(children)) => {
                // set tree to that quad
                self.tree = &children[quad.quad()];
                self.idx = 0;
                self.next()
            }
            // element doesn't fit in any quad & node has children
            (None, Some(ref children)) => {
                // set tree to noth west quad & add other quads to future trees
                if !self.tree.area.contains(self.element) {
                    return None;
                }
                self.tree = &children[0];
                self.future_trees.push(&children[1]);
                self.future_trees.push(&children[2]);
                self.future_trees.push(&children[3]);
                self.idx = 0;
                self.next()
            }
            // element has no children
            (_, None) => {
                // pop tree out of future list and if empty stop iterator
                match self.future_trees.pop() {
                    Some(tree) => {
                        self.tree = tree;
                        self.idx = 0;
                        self.next()
                    }
                    None => None,
                }
            }
        }
    }
}

#[cfg(test)]
mod quad_tests {
    use super::{QuadTree, AABB};
    use crate::shape::Shape;

    #[test]
    fn simple_test() {
        let rect_left_up = Shape::new_rect(10.0, 60.0, 15.0, 70.0, 0);
        let circle_left_up = Shape::new_circle(40.0, 90.0, 2.0, 1);
        let circle_left_down = Shape::new_circle(35.0, 30.0, 5.0, 2);
        let circle_right_up = Shape::new_circle(60.0, 80.0, 5.0, 3);
        let line_right_down = Shape::new_line(65.0, 20.0, 75.0, 30.0, 4);

        let mut qt: QuadTree<Shape> = QuadTree::new(AABB::new(50.0, 50.0, 50.0, 50.0));
        qt.insert(&line_right_down);
        qt.insert(&rect_left_up);
        qt.insert(&circle_left_up);
        qt.insert(&circle_left_down);
        qt.insert(&circle_right_up);

        let in_range: Vec<_> = qt.query(&rect_left_up).collect();
        assert_eq!(in_range.contains(&&circle_left_up), true);
        assert_eq!(in_range.contains(&&rect_left_up), true);
        assert_eq!(in_range.contains(&&line_right_down), false);
        assert_eq!(in_range.contains(&&circle_right_up), false);
        assert_eq!(in_range.contains(&&circle_left_down), false);
    }
}
