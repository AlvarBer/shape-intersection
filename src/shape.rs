use crate::quadtree::{Bounded, AABB};
use crate::vector2::Vector2;
use std::collections::HashMap;
use std::fmt;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Circle {
    pub id: u32,
    pub center: Vector2,
    pub radius: f64,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Rectangle {
    pub id: u32,
    pub left_down: Vector2,
    pub right_up: Vector2,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Line {
    pub id: u32,
    pub start: Vector2,
    pub end: Vector2,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Shape {
    Circle(Circle),
    Rectangle(Rectangle),
    Line(Line),
}

pub fn find_intersections(shapes: Vec<&Shape>) -> HashMap<u32, Vec<&Shape>> {
    let mut result = HashMap::new();
    for shape in shapes.clone() {
        let mut collisions = Vec::new();
        for other in shapes.clone() {
            if other != shape && do_intersect(&shape, &other) {
                collisions.push(other);
            }
        }
        result.insert(shape.get_id(), collisions);
    }
    result
}

pub fn do_intersect(first: &Shape, second: &Shape) -> bool {
    use self::Shape::*;
    match (first, second) {
        (Circle(circ_a), Circle(circ_b)) => {
            circ_a.center.distance(&circ_b.center) < circ_a.radius + circ_b.radius
        }
        (Rectangle(rect_a), Rectangle(rect_b)) => {
            // Assuming rectangles are axis aligned we can check just for corners
            !(rect_a.left_down.y >= rect_b.right_up.y
                || rect_b.left_down.y >= rect_a.right_up.y
                || rect_a.left_down.x >= rect_b.right_up.x
                || rect_b.left_down.x >= rect_a.right_up.x)
        }
        (Line(line_a), Line(line_b)) => line_a.collides_with(&line_b),
        (Circle(circle), Line(line)) | (Line(line), Circle(circle)) => {
            line.distance_to_point(circle.center) <= circle.radius
        }
        (Circle(circle), Rectangle(rect)) | (Rectangle(rect), Circle(circle)) => {
            // We can use the previous algorithm for all sides plus edge case of contained rect
            rect.contains(circle.center)
                || rect
                    .sides()
                    .iter()
                    .map(|side| side.distance_to_point(circle.center) <= circle.radius)
                    .any(|collision| collision)
        }
        (Rectangle(rect), Line(line)) | (Line(line), Rectangle(rect)) => {
            // Similar situation to last one, but using the segment collision algorithm
            // for every side
            rect.contains(line.start)
                || rect.contains(line.end)
                || rect
                    .sides()
                    .iter()
                    .map(|rect_side| line.collides_with(rect_side))
                    .any(|collides| collides)
        }
    }
}

impl Shape {
    pub fn get_id(&self) -> u32 {
        match self {
            Shape::Circle(circle) => circle.id,
            Shape::Rectangle(rect) => rect.id,
            Shape::Line(line) => line.id,
        }
    }

    pub fn new_line(start_x: f64, start_y: f64, end_x: f64, end_y: f64, id: u32) -> Self {
        Shape::Line(Line {
            start: Vector2::new(start_x, start_y),
            end: Vector2::new(end_x, end_y),
            id,
        })
    }

    pub fn new_circle(center_x: f64, center_y: f64, radius: f64, id: u32) -> Self {
        Shape::Circle(Circle {
            center: Vector2::new(center_x, center_y),
            radius,
            id,
        })
    }

    pub fn new_rect(
        left_down_x: f64,
        left_down_y: f64,
        right_up_x: f64,
        right_up_y: f64,
        id: u32,
    ) -> Self {
        Shape::Rectangle(Rectangle {
            left_down: Vector2::new(left_down_x, left_down_y),
            right_up: Vector2::new(right_up_x, right_up_y),
            id,
        })
    }
}

impl Bounded for Shape {
    fn bounds(&self) -> AABB {
        match self {
            Shape::Circle(circle) => AABB {
                center: circle.center,
                extents: Vector2::new(circle.radius, circle.radius),
            },
            Shape::Line(line) => {
                let half_diagonal = (line.start - line.end) * 0.5;
                let center = line.end + half_diagonal;
                let extents = half_diagonal.abs();
                AABB { center, extents }
            }
            Shape::Rectangle(rect) => {
                let extents = (rect.right_up - rect.left_down) * 0.5;
                let center = rect.left_down + extents;
                AABB { center, extents }
            }
        }
    }
}

impl fmt::Display for Shape {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Shape::Circle(circle) => write!(f, "•: {} ◯: {}", circle.center, circle.radius),
            Shape::Rectangle(rect) => write!(f, "└: {} ┐: {}", rect.left_down, rect.right_up),
            Shape::Line(line) => write!(f, "{} → {}", line.start, line.end),
        }
    }
}

impl Line {
    /// Return None rather than NaN on vertical lines
    pub fn slope(&self) -> Option<f64> {
        if self.end.x - self.start.x != 0.0 {
            Some((self.end.y - self.start.y) / (self.end.x - self.start.x))
        } else {
            None
        }
    }

    pub fn general_form(&self) -> (f64, f64, f64) {
        let a = match self.slope() {
            Some(slope) if slope == 0.0 => 0.0,
            Some(_) | None => 1.0,
        };
        let b = match self.slope() {
            Some(slope) => -(1.0 / slope),
            None => 0.0,
        };
        let c = match self.slope() {
            Some(slope) => self.start.x - self.start.y / slope,
            None => -self.start.x,
        };
        (a, b, c)
    }

    /// https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    pub fn distance_to_point(&self, point: Vector2) -> f64 {
        let (a, b, c) = self.general_form();
        let distance = (a * point.x + b * point.y + c).abs() / (a * a + b * b).sqrt();
        distance
    }

    /// Assuming point is colinear
    pub fn contains(&self, point: Vector2) -> bool {
        point.x <= self.start.x.max(self.end.x)
            && point.x >= self.start.x.min(self.end.x)
            && point.y <= self.start.y.max(self.end.y)
            && point.y >= self.start.y.min(self.end.y)
    }

    pub fn collides_with(&self, other: &Self) -> bool {
        let a1 = self.end.y - self.start.y;
        let b1 = self.start.x - self.end.x;
        let c1 = a1 * self.start.x + b1 * self.start.y;

        let a2 = other.end.y - other.start.y;
        let b2 = other.start.x - other.end.x;
        let c2 = a2 * other.start.x + b2 * other.start.y;

        let delta = a1 * b2 - a2 * b1;

        if delta == 0.0 {
            self.general_form() == other.general_form()
                && (self.contains(other.start) || self.contains(other.end))
        } else {
            let intersect = Vector2::new((b2 * c1 - b1 * c2) / delta, (a1 * c2 - a2 * c1) / delta);
            self.contains(intersect) && other.contains(intersect)
        }
    }

    pub fn new(start: Vector2, end: Vector2, id: u32) -> Self {
        Line { start, end, id }
    }
}

impl Rectangle {
    fn sides(&self) -> ([Line; 4]) {
        let right_bot = Vector2::new(self.right_up.x, self.left_down.y);
        let left_up = Vector2::new(self.left_down.x, self.right_up.y);
        let up = Line::new(left_up, self.right_up, 0);
        let right = Line::new(self.right_up, right_bot, 0);
        let down = Line::new(right_bot, self.left_down, 0);
        let left = Line::new(self.left_down, left_up, 0);
        [up, right, down, left]
    }

    fn contains(&self, point: Vector2) -> bool {
        point.x >= self.left_down.x
            && point.y >= self.left_down.y
            && point.x <= self.right_up.x
            && point.y <= self.right_up.y
    }
}

#[cfg(test)]
mod shape_tests {
    use super::*;

    #[test]
    fn test_general_form() {
        let diagonal = Line::new(Vector2::new(0.0, 0.0), Vector2::new(1.0, 1.0), 0);
        assert_eq!(diagonal.general_form(), (1.0, -1.0, 0.0));
        let line = Line::new(Vector2::new(0.0, 0.0), Vector2::new(1.0, 2.0), 0);
        assert_eq!(line.general_form(), (1.0, -0.5, 0.0));
        let vertical = Line::new(Vector2::new(3.0, 3.0), Vector2::new(3.0, 5.0), 0);
        assert_eq!(vertical.general_form(), (1.0, 0.0, -3.0));
    }

    #[test]
    fn overlapping_circles() {
        let c1 = Shape::new_circle(0.0, 0.0, 0.5, 0);
        let c2 = Shape::new_circle(0.73, 0.63, 0.5, 0);
        assert_eq!(do_intersect(&c1, &c2), true);
    }

    #[test]
    fn same_circle() {
        let c1 = Shape::new_circle(3.0, 5.0, 7.0, 0);
        assert_eq!(do_intersect(&c1, &c1), true);
    }

    #[test]
    fn concentric_circles() {
        let c1 = Shape::new_circle(3.0, 5.0, 7.0, 0);
        let c2 = Shape::new_circle(3.0, 5.0, 2.0, 0);
        assert_eq!(do_intersect(&c1, &c2), true);
    }

    #[test]
    fn close_non_overlapping_circles() {
        let c1 = Shape::new_circle(0.0, 0.0, 0.5, 0);
        let c2 = Shape::new_circle(0.76, 0.66, 0.5, 0);
        assert_eq!(do_intersect(&c1, &c2), false);
    }

    #[test]
    fn crossing_lines() {
        let l1 = Shape::new_line(0.0, 0.0, 1.0, 1.0, 0);
        let l2 = Shape::new_line(1.0, 0.0, 0.0, 1.0, 0);
        assert_eq!(do_intersect(&l1, &l2), true);
    }

    #[test]
    fn parallel_lines() {
        let l1 = Shape::new_line(0.0, 0.0, 1.0, 1.0, 0);
        let l2 = Shape::new_line(0.1, 0.0, 1.1, 1.0, 0);
        assert_eq!(do_intersect(&l1, &l2), false);
    }

    #[test]
    fn discontinuous_line() {
        let l1 = Shape::new_line(0.0, 0.0, 1.0, 1.0, 0);
        let l2 = Shape::new_line(1.5, 1.5, 2.5, 2.5, 0);
        assert_eq!(do_intersect(&l1, &l2), false);
    }

    #[test]
    fn contained_lines() {
        let l1 = Shape::new_line(0.0, 0.0, 1.0, 1.0, 0);
        let l2 = Shape::new_line(0.25, 0.25, 0.75, 0.75, 0);
        assert_eq!(do_intersect(&l1, &l2), true);
    }

    #[test]
    fn same_line() {
        let l1 = Shape::new_line(0.0, 0.0, 1.0, 1.0, 0);
        assert_eq!(do_intersect(&l1, &l1), true);
    }

    #[test]
    fn crossing_not_overlapping_lines() {
        let l1 = Shape::new_line(0.0, 0.0, 0.49, 0.49, 0);
        let l2 = Shape::new_line(1.0, 0.0, 0.51, 0.49, 0);
        assert_eq!(do_intersect(&l1, &l2), false);
    }

    #[test]
    fn faraway_lines() {
        let l1 = Shape::new_line(0.0, 0.0, 4.0, 4.0, 0);
        let l2 = Shape::new_line(6.0, 0.0, 4.0, 2.0, 0);
        assert_eq!(do_intersect(&l1, &l2), false);
    }

    #[test]
    fn tangential_line_and_circle() {
        let line = Shape::new_line(0.21, 0.98, -0.73, 0.04, 0);
        let circle = Shape::new_circle(0.0, 0.0, 0.5, 0);
        assert_eq!(do_intersect(&line, &circle), false);
    }

    #[test]
    fn tangential_overlapping_line_and_circle() {
        let line = Shape::new_line(-0.569, -0.004, 0.042, 0.607, 0);
        let circle = Shape::new_circle(0.0, 0.0, 0.5, 0);
        assert_eq!(do_intersect(&line, &circle), true);
    }

    #[test]
    fn tangential_overlapping_longer_line_and_circle() {
        let line = Shape::new_line(0.802, 1.366, -1.543, -0.978, 0);
        let circle = Shape::new_circle(0.0, 0.0, 0.5, 0);
        assert_eq!(do_intersect(&line, &circle), true);
    }

    #[test]
    fn line_inside_circle() {
        let line = Shape::new_line(0.25, 0.25, 0.75, 0.75, 0);
        let circle = Shape::new_circle(0.0, 0.0, 1.0, 0);
        assert_eq!(do_intersect(&line, &circle), true);
    }

    #[test]
    fn close_rectangles() {
        let rect1 = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let rect2 = Shape::new_rect(0.6, 0.4, 1.6, 1.4, 0);
        assert_eq!(do_intersect(&rect1, &rect2), false);
    }

    #[test]
    fn overlapping_corner_rectangles() {
        let rect1 = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let rect2 = Shape::new_rect(0.45, 0.45, 1.45, 1.45, 0);
        assert_eq!(do_intersect(&rect1, &rect2), true);
    }

    #[test]
    fn rect_inside_rect() {
        let rect1 = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let rect2 = Shape::new_rect(-0.05, 0.05, 0.45, 0.35, 0);
        assert_eq!(do_intersect(&rect1, &rect2), true);
    }

    #[test]
    fn close_circle_and_rect() {
        let rect = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let circ = Shape::new_circle(0.61, 0.2, 0.1, 0);
        assert_eq!(do_intersect(&rect, &circ), false);
    }

    #[test]
    fn overlapping_corner_circle_and_rect() {
        let rect = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let circ = Shape::new_circle(0.565, 0.565, 0.1, 0);
        assert_eq!(do_intersect(&rect, &circ), true);
    }

    #[test]
    fn close_corner_line_and_rect() {
        let rect = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let line = Shape::new_line(-0.726, 0.314, -0.213, 0.827, 0);
        assert_eq!(do_intersect(&rect, &line), false);
    }

    #[test]
    fn overlapping_corner_line_and_rect() {
        let rect = Shape::new_rect(-0.5, -0.5, 0.5, 0.5, 0);
        let line = Shape::new_line(-0.117, 0.731, -0.634, 0.214, 0);
        assert_eq!(do_intersect(&rect, &line), true);
    }
}
